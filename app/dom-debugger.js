// to depend on a bower installed component:
// define(['component/componentName/file'])

define(["jquery", "node-details",  "ruler"], function($, NodeDetails, ruler) {

	var currentNodeDetailsView = null;

	var initDebuggerView = function() {
		var mouseeffect = "html.DOMDEBUGGER_UNSELECTED *:hover > *:hover { outline: 1px solid green; opacity:1; } html.DOMDEBUGGER_UNSELECTED *:hover > *:not(:hover) {outline: 1px solid darkgray;opacity:0.75;}";
		var mousestyle = '<style id="DOM-DEBUGGER-OUTLINE" type="text/css">' + mouseeffect + "</style>";
		var rulerstyle = '<link id="DOM-DEBUGGER-RULER" rel="stylesheet" type="text/css" href="app/../components/ruler/ruler.css">';
		$("head").append(mousestyle);
		$("head").append(rulerstyle);
		$('html').addClass('DOMDEBUGGER_UNSELECTED');
		$('body').ruler();  
	}

	/**
	* Draws a bounding rectangle  over the selected dom node.
	* this is made by injecting a div with the same width and height 
	*/
	var showNodeDetails = function(domNode){
		currentNodeDetailsView = new NodeDetails(domNode, $("body"));
		currentNodeDetailsView.render();
		$('html').removeClass('DOMDEBUGGER_UNSELECTED');
	};

	/**
	* Hides the bounding box that was previously drawn 
	*/
	var hideNodeDetails = function(){
		if(currentNodeDetailsView !== null){
			currentNodeDetailsView.destroy();
			currentNodeDetailsView = null;
			$('html').addClass('DOMDEBUGGER_UNSELECTED');
		}
	};

	return {

		init: function(){
			initDebuggerView();
			$("body").on("contextmenu", function(event){
				var node = $(event.target);
				event.preventDefault();
				hideNodeDetails();
				if(!node.hasClass("dom-debugger-injected-elements")){
					showNodeDetails(node);
				}
			});
		}

	};

});
