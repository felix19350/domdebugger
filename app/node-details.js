/**
* This is the view that renders the details for a selected dom node.
*/

define(["jquery", "handlebars", "text!templates/css-details.htm"], function($, Handlebars, cssDetails){

	"use strict";

	//Behaves like a constructor function. Returns an object with the 
	// properties and methods we wish to expose.

	//Private view that outputs the details of the dom node
	var CssEditorView = function(jqDomNode){

		var computedStyle = window.getComputedStyle(jqDomNode.get(0));

		return {
			detailsDiv: null,

			render: function(){
				var i = 0;
				var cssProperties = [];

				//Do a little parsing so we can 
				for(i = 0 ; i < computedStyle.length; i++){
					var prop = computedStyle[i];
					var value = computedStyle[prop];
					if(computedStyle.hasOwnProperty(prop)){
						cssProperties.push({property: prop, value: value});
					}
				}
				console.log(cssProperties);
				var template = Handlebars.compile(cssDetails);
				this.detailsDiv =  $(template({
					nodeName: jqDomNode.get(0).nodeName,
					cssProperties: cssProperties
				})); //Render the template and turn it into a jquery enhanced element

				return this.detailsDiv;
			},

			destroy: function(){
				if(this.detailsDiv !== null){
					this.detailsDiv.remove();
					$('.dom-debugger-injected-elements').remove();
					this.detailsDiv = null;
				}
			}
		};
	};

	function parse(prop){
	    var p = parseFloat(prop), q = prop.replace(/^[\-\d\.]+/,'');
	    return isNaN(p) ? { v: q, u: ''} : { v: p, u: q };
	}
	function changeSignal(propValue) { // "-3cm"
		var parsed = parse(propValue);
		return (-1*parsed.v) + "" + parsed.u;
	}
	function positiveOrZero(propValue) {
		var parsed = parse(propValue);
		return (Math.max(0,parsed.v)) + "" + parsed.u;
	}
	function absolute(propValue) {
		var parsed = parse(propValue);
		return (Math.abs(parsed.v)) + "" + parsed.u;
	}

	return function(jqDomNode, jqTargetNode){
		// Includes margins, borders and paddings
		var outerWidth = jqDomNode.outerWidth(true) ;
		var outerHeight = jqDomNode.outerHeight(true);
		// Includes borders and paddings
		var outerWidthNoMargins = jqDomNode.outerWidth() ;
		var outerHeightNoMargins = jqDomNode.outerHeight();
		// includes paddings
		var innerWidth = jqDomNode.innerWidth() ;
		var innerHeight = jqDomNode.innerHeight();

		var width = jqDomNode.width() ;
		var height = jqDomNode.height();

		var position = jqDomNode.offset();
		var top = position.top + "px";
		var left = position.left + "px";

		// getComputedStyle always returns in pixels
		var cstyle = window.getComputedStyle(jqDomNode.get(0));
		var marginLeft = cstyle.marginLeft;
		var marginTop = cstyle.marginTop;
		var marginRight = cstyle.marginRight;
		var marginBottom = cstyle.marginBottom;

		var borderTopWidth = cstyle.borderTopWidth;
		var borderBottomWidth = cstyle.borderBottomWidth;
		var borderLeftWidth = cstyle.borderLeftWidth;
		var borderRightWidth = cstyle.borderRightWidth;

		var paddingTop = cstyle.paddingTop;
		var paddingBottom = cstyle.paddingBottom;
		var paddingLeft = cstyle.paddingLeft;
		var paddingRight = cstyle.paddingRight;

		//TODO: add the ability to customize the overlay css
		return {

			injectedNode: null,
			cssInfoOverlay: null,


			marginsTop: {
				"position": "absolute",
				"z-index":100,
				"top": "-" + positiveOrZero(marginTop),
				"left":  changeSignal(borderLeftWidth),
				"width": outerWidthNoMargins,
				"height": absolute(marginTop),
				"background-color" : (marginTop[0] == '-')? "lightcoral" : "lightgreen"
			},
			marginsLeft: {
				"position": "absolute",
				"z-index":100,
				"top": changeSignal(borderTopWidth),
				"left":   "-" + positiveOrZero(marginLeft),
				"width": absolute(marginLeft),
				"height": outerHeightNoMargins,
				"background-color" : (marginLeft[0] == '-')? "lightcoral" : "lightgreen"
			},
			marginsBottom: {
				"position": "absolute",
				"z-index":100,
				"bottom": "-" + positiveOrZero(marginBottom),
				"left":  changeSignal(borderLeftWidth),
				"width": outerWidthNoMargins,
				"height": absolute(marginBottom),
				"background-color" : (marginBottom[0] == '-')? "lightcoral" : "lightgreen"
			},
			marginsRight: {
				"position": "absolute",
				"z-index":100,
				"top": changeSignal(borderTopWidth),
				"right":   "-" + positiveOrZero(marginRight),
				"width": absolute(marginRight),
				"height": outerHeightNoMargins,
				"background-color" : (marginRight[0] == '-')? "lightcoral" : "lightgreen"
			},
			paddingCss: {
				"position": "absolute",
				"opacity": 0.5,
				"top": top,
				"left": left,
				"width": width,
				"height": height,
				"padding-top": jqDomNode[0].style.paddingTop,
				"padding-left": jqDomNode[0].style.paddingLeft,
				"padding-right": jqDomNode[0].style.paddingRight,
				"padding-bottom": jqDomNode[0].style.paddingBottom,
				"background-color": "lightblue",
				"border": jqDomNode[0].style.border,
				"border-left": jqDomNode[0].style.borderLeft,
				"border-right": jqDomNode[0].style.borderRight,
				"border-top": jqDomNode[0].style.borderTop,
				"border-bottom": jqDomNode[0].style.borderBottom,
			},
			innerCss:{
				"background-color": "lightyellow",
				"width": width,
				"height": height,
				"background-image" : "none"
			},


			render: function(){				
				//TODO: change this so that a decent templating engine is used. Directly writing this is downright ugly

				// Paint paddings
				this.injectedNode = $("<div class='dom-debugger-injected-elements'></div>");
				this.injectedNode.css(this.paddingCss);
				jqTargetNode.append(this.injectedNode);
				// Paint node
				jqTargetNode = this.injectedNode;
				this.injectedNode = $("<div class='dom-debugger-injected-elements'></div>");
				this.injectedNode.css(this.innerCss);
				jqTargetNode.append(this.injectedNode);

				// paint margins
				this.injectedNode = $("<div class='dom-debugger-injected-elements'></div>");
				this.injectedNode.css(this.marginsTop);
				jqTargetNode.append(this.injectedNode);
				this.injectedNode = $("<div class='dom-debugger-injected-elements'></div>");
				this.injectedNode.css(this.marginsLeft);
				jqTargetNode.append(this.injectedNode);
				this.injectedNode = $("<div class='dom-debugger-injected-elements'></div>");
				this.injectedNode.css(this.marginsBottom);
				jqTargetNode.append(this.injectedNode);
				this.injectedNode = $("<div class='dom-debugger-injected-elements'></div>");
				this.injectedNode.css(this.marginsRight);
				jqTargetNode.append(this.injectedNode);


				this.cssInfoOverlay = new CssEditorView(jqDomNode);
				jqTargetNode.append(this.cssInfoOverlay.render());
			},

			destroy: function(){
				if(this.injectedNode !== null){
					this.injectedNode.remove();
					this.injectedNode = null;
				}

				if(this.cssInfoOverlay !== null){
					this.cssInfoOverlay.destroy();
					this.cssInfoOverlay = null;
				}
			}
		};

	};
});