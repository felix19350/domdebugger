require.config({
  // make components more sensible
  // expose jquery 
  paths: {
    "components": "../components",
    "templates": "../templates",
    "jquery": "../components/jquery/jquery",
    "handlebars": "../components/handlebars/handlebars",
    "text": "../components/text/text", //Handlebars text plugin
    "modernizr": "../components/modernizr/modernizr",
    "ruler": "../components/ruler/ruler"
  },

  shim: {
    'ruler': {
            deps: ['jquery', 'modernizr'],
            exports: 'ruler'
        },
        'handlebars':{
            exports: 'Handlebars'
        }
  }

});
require(['dom-debugger'], function(domDebugger){
	console.log("loaded DOM Debugger");
	domDebugger.init();
});





